import requests
import os


def datacenter_manager_url():
    try:
        from django.conf import settings
    except ImportError:
        return os.getenv(
            "DATACENTER_MANAGER_API_URL",
            default="https://datacenter-manager-api.example.com/",
        )

    return settings.DATACENTER_MANAGER_API_URL


def datacenter_manager_default_credentials():
    try:
        from django.conf import settings
    except ImportError:
        username = os.getenv("DATACENTER_MANAGER_API_USER", default="user")
        password = os.getenv("DATACENTER_MANAGER_API_PASSWD", default="secret")
        return username, password

    username = settings.DATACENTER_MANAGER_API_USER
    password = settings.DATACENTER_MANAGER_API_PASSWD

    return username, password


def datacenter_manager_head(
    endpoint: str, api_user: str, api_password: str, params=None
):
    if params is None:
        params = dict()

    response = requests.head(
        f"{datacenter_manager_url()}/{endpoint}",
        headers={
            "Authorization": "ApiKey {}:{}".format(
                api_user,
                api_password,
            ),
            "Content-Type": "application/json",
            "Accept": "application/json",
        },
        params=params,
    )

    return response.headers


def datacenter_manager_get_data(
    endpoint: str, api_user: str, api_password: str, params=None
):
    if params is None:
        params = dict()

    response = requests.get(
        f"{datacenter_manager_url()}/{endpoint}",
        headers={
            "Authorization": "ApiKey {}:{}".format(
                api_user,
                api_password,
            ),
            "Content-Type": "application/json",
            "Accept": "application/json",
        },
        params=params,
    )

    return response.json()


def datacenter_manager_post_data(
    endpoint: str, api_user: str, api_password: str, params=None, data=None
):
    if params is None:
        params = dict()

    if data is None:
        data = dict()

    response = requests.post(
        f"{datacenter_manager_url()}/{endpoint}",
        headers={
            "Authorization": "ApiKey {}:{}".format(
                api_user,
                api_password,
            ),
            "Content-Type": "application/json",
            "Accept": "application/json",
        },
        params=params,
        json=data,
    )

    return response.json()


def datacenter_manager_patch_data(
    endpoint: str, api_user: str, api_password: str, params=None, data=None
):
    if params is None:
        params = dict()

    if data is None:
        data = dict()

    response = requests.patch(
        f"{datacenter_manager_url()}/{endpoint}",
        headers={
            "Authorization": "ApiKey {}:{}".format(
                api_user,
                api_password,
            ),
            "Content-Type": "application/json",
            "Accept": "application/json",
        },
        params=params,
        json=data,
    )

    return response.json()


def datacenter_manager_put_data(
    endpoint: str, api_user: str, api_password: str, params=None, data=None
):
    if params is None:
        params = dict()

    if data is None:
        data = dict()

    response = requests.patch(
        f"{datacenter_manager_url()}/{endpoint}",
        headers={
            "Authorization": "ApiKey {}:{}".format(
                api_user,
                api_password,
            ),
            "Content-Type": "application/json",
            "Accept": "application/json",
        },
        params=params,
        json=data,
    )

    return response.json()


def datacenter_manager_delete_data(
    endpoint: str, api_user: str, api_password: str, params=None
):
    if params is None:
        params = dict()

    response = requests.delete(
        f"{datacenter_manager_url()}/{endpoint}",
        headers={
            "Authorization": "ApiKey {}:{}".format(
                api_user,
                api_password,
            ),
            "Content-Type": "application/json",
            "Accept": "application/json",
        },
        params=params,
    )

    if response.status_code == 200:
        return True

    return False


def datacenter_manager_get_api_credentials_for_customer(mdat_id: int):
    username, password = datacenter_manager_default_credentials()

    if username != mdat_id:
        response = datacenter_manager_get_data(
            f"v1/apiauthcustomerapikey/{mdat_id}",
            username,
            password,
        )

        username = str(response["user"]["id"])
        password = response["key"]

    return username, password


def datacenter_manager_get_ipsubnets(mdat_id: int):
    username, password = datacenter_manager_get_api_credentials_for_customer(mdat_id)

    response = datacenter_manager_get_data(
        "v1/ipsubnet",
        username,
        password,
    )

    if "objects" not in response:
        return list()

    return response["objects"]


def datacenter_manager_get_ipsubnet(mdat_id: int, subnet_id: int):
    username, password = datacenter_manager_get_api_credentials_for_customer(mdat_id)

    response = datacenter_manager_get_data(
        f"v1/ipsubnet/{subnet_id}",
        username,
        password,
    )

    return response


def datacenter_manager_get_vlans(mdat_id: int):
    username, password = datacenter_manager_get_api_credentials_for_customer(mdat_id)

    response = datacenter_manager_get_data(
        "v1/vlan",
        username,
        password,
    )

    if "objects" not in response:
        return list()

    return response["objects"]


def datacenter_manager_get_vlan(mdat_id: int, vlan_id: int):
    username, password = datacenter_manager_get_api_credentials_for_customer(mdat_id)

    response = datacenter_manager_get_data(
        f"v1/vlan/{vlan_id}",
        username,
        password,
    )

    return response["vlan_id"]


def datacenter_manager_get_ip_info(mdat_id: int, ip_id: int):
    username, password = datacenter_manager_get_api_credentials_for_customer(mdat_id)

    response = datacenter_manager_get_data(
        f"v1/ipaddress/{ip_id}",
        username,
        password,
    )

    return response


def datacenter_manager_get_dhcpkea(mdat_id: int, instance_id: int, params=None):
    username, password = datacenter_manager_get_api_credentials_for_customer(mdat_id)

    response = datacenter_manager_get_data(
        f"v1/dhcpkea/{instance_id}",
        username,
        password,
        params,
    )

    return response
